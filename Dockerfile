FROM archlinux
RUN useradd -ms /bin/bash admin
COPY --chown=admin:admin ./sb/ /app/
EXPOSE 21025
VOLUME /app
WORKDIR /app
RUN pacman -Sy --noconfirm lib32-gcc-libs libvorbis
RUN chmod 755 /app
USER admin
CMD sh start.sh
