# Starbound server without steam
A starbound server which doesn't use steam, cuz steam needs credentials.

## Requirements
- A copy of Starbound for linux
- Docker (duh)

## Install
- Go to your Starbound linux installation folder and copy all the folders into the sb folder of this repo.
- Edit the folders in there like a normal server. (edit config files, add mods, etc)
- Launch `docker build -t starbound .` This might take a while. Luckily, you'll normally need to run this only once. (unless you modify files in `sb`)

## Run
- Launch `docker run -d -p 21025:21025/tcp starbound`. (ports option is here if different port is needed. Just change the first one.)

## Troubleshotting
### Connection refused!
Check the ports. Even localhost needs open ports in router.

### Override thing?
Check the box "allow assets mismatch" in your game's options

### Be admin of server?
Just check `starbound_server.config` at the empty `serverUsers` object and add something like this:
```
"serverUsers" : {
    "username" : {
        "admin" : true,
        "password" : "userpassword"
    }
},
```

### Why archlinux? Arch sucks!
It's your opinion... Though, arch is apparently the only lightweight docker image that still have lib32 gcc libs in their package repos and these are necessary for the Starbound server to run. If you could manage to make this work with another docker image, feel free to tell!
